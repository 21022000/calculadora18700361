const numeros = document.getElementsByName("num");
const operadores = document.getElementsByName("opera");
const limpiar = document.getElementsByName("borrar")[0];
const igual = document.getElementsByName("igual")[0];
var mostrar = document.getElementById("resultado");
var opac='';
var opan='';
var operacion=undefined;
console.log(numeros);
console.log(operadores);
console.log(limpiar);
console.log(igual);
console.log(mostrar);

numeros.forEach(function(boton) {
	boton.addEventListener('click',function(){
		desplegar(boton.innerText);
	})
});

operadores.forEach(function(boton){
	boton.addEventListener('click',function(){
		operador(boton.innerText);
		
	})

})

igual.addEventListener('click',function(){
	calculo();
	actualizar();
});

limpiar.addEventListener('click',function(){
	limpiarcaja();
	actualizar();
});

function operador(ope) {
	if(opac === '') return;
	if(opan !== ''){
		calculo()

	}
	operacion = ope.toString();
	opan = opac;
	opac = '';

}

function calculo() {
	var calcular;
	const ante = parseFloat(opan);
	const actu = parseFloat(opac);
	if(isNaN(ante)||isNaN(actu)) return;
	switch(operacion){
		case '+':
		calcular = ante + actu;
		break;
		case '-':
		calcular = ante - actu;
		break;
		case 'x':
		calcular = ante * actu;
		break;
		case '/':
		calcular = ante / actu;
		break;
		default:
		return;

	}
	opac = calcular;
	operacion = undefined;
	opan = '';
}
function desplegar(num){
	opac = opac.toString() + num.toString();
	actualizar();
}

function limpiarcaja(){
	opac ='';
	opan ='';
	operacion = undefined;
}
function actualizar() {
	mostrar.value = opac;
}

limpiarcaja();

